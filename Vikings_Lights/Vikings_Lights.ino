
#include <Adafruit_NeoPixel.h>

//#### LED variables ####
const int LedPin = 14;
const int NumLeds = 300;
//const int NumLeds = 50;
const int NumRows = 4;

const int LedPerRow = NumLeds/NumRows;

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NumLeds, LedPin, NEO_GRB + NEO_KHZ800); //(# of pixels in strip, pin #, pixel type flags)

//const strip.Color VikesPurple = strip.Color(0xB7,0x00,0xFE);
//uint32_t VikingsPurple = strip.gamma32(strip.ColorHSV(50462, 237, 255));
uint32_t VikingsPurple = strip.gamma32(strip.Color(197, 0, 255));
uint32_t VikingsGold = strip.gamma32(strip.ColorHSV(7864, 232, 255));

//uint32_t PackersGreen = strip.gamma32(strip.ColorHSV(25486, 255, 255));
uint32_t PackersGreen = strip.gamma32(strip.Color(0, 255, 0));

//**** SETUP ****
void setup()
{

  Serial.begin(9600);
  //initialize LED strip
  strip.begin(); // initialize LED strip
  strip.clear(); // turn off all LEDs
  strip.setBrightness(100);
}

void showStrip() {
 #ifdef ADAFRUIT_NEOPIXEL_H
   // NeoPixel
   strip.show();
 #endif
 #ifndef ADAFRUIT_NEOPIXEL_H
   // FastLED
   FastLED.show();
 #endif
}

void setPixel(int Pixel, byte red, byte green, byte blue) {
 #ifdef ADAFRUIT_NEOPIXEL_H
   // NeoPixel
   strip.setPixelColor(Pixel, strip.Color(red, green, blue));
 #endif
 #ifndef ADAFRUIT_NEOPIXEL_H
   // FastLED
   leds[Pixel].r = red;
   leds[Pixel].g = green;
   leds[Pixel].b = blue;
 #endif
}

void setAll(byte red, byte green, byte blue) {
  for(int i = 0; i < NumLeds; i++ ) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}

void FadeIn(byte red, byte green, byte blue){
  float r, g, b;
     
  for(int k = 0; k < 256; k=k+1) {
    r = (k/256.0)*red;
    g = (k/256.0)*green;
    b = (k/256.0)*blue;
    setAll(r,g,b);
    showStrip();
  }
}

void FadeOut(byte red, byte green, byte blue){
  float r, g, b;     
  for(int k = 255; k >= 0; k=k-2) {
    r = (k/256.0)*red;
    g = (k/256.0)*green;
    b = (k/256.0)*blue;
    setAll(r,g,b);
      showStrip();
  }
}


void FightLeftRight(uint32_t LeftColor, uint32_t RightColor, int PixelsPerPush=4, int MaxFights= 10, int Delay=10){
  //Meet in the middle
  for(int pixel=0; pixel < (LedPerRow/2); pixel++){
    for(int Row = 0; Row<=NumRows; Row++ ){
      //figure out far left and right pixel
      int Leftmost = Row*LedPerRow;
      int Rightmost = Leftmost + LedPerRow -1; //-1 = 0 based
      int LeftPixel = Leftmost + pixel;
      int RightPixel = Rightmost - pixel;
      strip.setPixelColor(LeftPixel, LeftColor);
      strip.setPixelColor(RightPixel, RightColor);
      strip.show();
      delay(Delay);
    }
  }
  
  int position = LedPerRow/2; //starting in the middle
  int movement =0;
  
  //pick a random number of fights
  for(int fight=random(5,MaxFights); fight<=MaxFights; fight++){
    Serial.println((String)"were fighting:"+fight+" times:");
    Serial.println((String)"position:" + position);
    
    if (random(100) <50 ){ //go left
        movement = -1;
      }else{ //go right
        movement = 1;
      }
    for (int i = 0; i< PixelsPerPush; i++){
      position += movement;
      for(int Row = 0; Row<=NumRows; Row++ ){
      //figure out far left and right pixel
      int Leftmost = Row*LedPerRow;
      int Rightmost = Leftmost + LedPerRow -1; //-1 = 0 based
      int LeftPixel = Leftmost + position -1; //-1 = 0 based
      int RightPixel = Rightmost - (LedPerRow-position-1); //-1 = 0 based
      
      strip.setPixelColor(LeftPixel, LeftColor);
      strip.setPixelColor(RightPixel, RightColor);
      strip.show();
      delay(Delay);
      }
    }
  }
}

void FillLeftRight(uint32_t LeftColor, int Delay=10){
  //Meet in the middle
  for(int column=0; column < LedPerRow; column++){
    for(int Row = 0; Row<NumRows;Row++ ){
      int LeftPixel = column+(Row*LedPerRow);
      strip.setPixelColor(LeftPixel, LeftColor);
    }
    strip.show();
    delay(Delay);
  }
}

void FillRightLeft(uint32_t RightColor, int Delay=10){
  //Meet in the middle
  for(int column=LedPerRow; column >= 0; column--){
    for(int Row = 0; Row<NumRows;Row++ ){
      int LeftPixel = column+(Row*LedPerRow)-1; //-1 for zero index
      strip.setPixelColor(LeftPixel, RightColor);
    }
    strip.show();
    delay(Delay);
  }
}

// **** MAIN ****
void loop()
{
     
  FadeIn(0xB7, 0x00, 0xFE);
  FadeOut(0xB7, 0x00, 0xFE);
  
  FightLeftRight(VikingsPurple, PackersGreen, 10, 20);
  
  FadeIn(0xB7, 0x00, 0xFE);
  FadeOut(0xB7, 0x00, 0xFE);
  
  FillLeftRight(PackersGreen);
  FillRightLeft(VikingsGold);
  FadeOut(0xFE, 0xD4, 0x00);
}
